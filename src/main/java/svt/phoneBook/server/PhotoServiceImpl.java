package svt.phoneBook.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import svt.phoneBook.HibernateUtil;
import svt.phoneBook.client.PhotoService;
import svt.phoneBook.client.dto.PhotoDTO;
import svt.phoneBook.shared.Photo;

import java.util.ArrayList;
import java.util.List;

public class PhotoServiceImpl extends RemoteServiceServlet implements PhotoService {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public String getMessage(String msg) {
        return "Hello";
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public PhotoDTO getPhotoById(String id) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Photo.class);
        criteria.add(Restrictions.eq("id", Integer.parseInt(id)));
        List<Photo> photoCriteria = criteria.list();
//        Photo  photo = (Photo) session.createQuery("from Photo where contactId = " + id).uniqueResult();
        PhotoDTO photoDTO = new PhotoDTO();
        if(photoCriteria != null && !photoCriteria.isEmpty()){
            Photo photo = photoCriteria.get(0);
            photoDTO = PhotoDTO.ofDTO(photo);
        }
        session.close();
        return photoDTO;
    }



    @SuppressWarnings("deprecation")
    public PhotoDTO getPhotoByContactId(int id) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Photo.class);
        criteria.add(Restrictions.eq("contactId", id));
        criteria.addOrder(Order.asc("id"));
        criteria.setMaxResults(1);
        Photo photo = (Photo) criteria.uniqueResult();
        PhotoDTO photoDTO = new PhotoDTO();
        if (photo != null) {
            photoDTO = PhotoDTO.ofDTO(photo);
        }
        session.close();
        return photoDTO;
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public List<PhotoDTO> getAllPhoto() {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Photo.class);
        List<Photo> photoCriteria = criteria.list();
//        List<Photo> photos = new ArrayList<>(session.createQuery("from Photo").list());
        List<PhotoDTO> photoDTOS = new ArrayList<PhotoDTO>(photoCriteria != null ? photoCriteria.size() : 0);
        if(photoCriteria != null){
            for (Photo photo : photoCriteria) {
                System.out.println(photo);
                photoDTOS.add(PhotoDTO.ofDTO(photo));
                System.out.println(photoDTOS);
            }
        }
        session.close();
        return photoDTOS;
    }

    public void savePhoto(int id, String photoName){
        Photo photo = new Photo();
        photo.setContactId(id);
        photo.setPhoto(photoName);

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(photo);
        session.getTransaction().commit();
        session.close();
    }



}
