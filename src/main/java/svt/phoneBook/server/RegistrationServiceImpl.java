package svt.phoneBook.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import svt.phoneBook.HibernateUtil;
import svt.phoneBook.client.registration.RegistrationService;
import svt.phoneBook.shared.User;

public class RegistrationServiceImpl extends RemoteServiceServlet implements RegistrationService {
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public String saveNewUser(String name, String password) {
        Transaction tx = null;
        Session session = sessionFactory.openSession();
        try {
            tx = session.beginTransaction();
            User user = new User();
            user.setName(name);
            user.setPassword(password);
            user.setRole("User");
            session.save(user);
            tx.commit();
            session.close();
            return String.valueOf(user.getId());
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
                session.close();
            }
            e.printStackTrace();
        }
        return null;
    }
}
