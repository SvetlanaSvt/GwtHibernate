package svt.phoneBook.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import svt.phoneBook.HibernateUtil;
import svt.phoneBook.client.ContactService;
import svt.phoneBook.client.dto.ContactDTO;
import svt.phoneBook.shared.Contact;
import svt.phoneBook.shared.Photo;

import java.util.ArrayList;
import java.util.List;

public class ContactServiceImpl extends RemoteServiceServlet implements ContactService {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    // Implementation of sample interface method
    public String getMessage(String msg) {
        return "Client said: \"" + msg + "\"<br>Server answered: \"Hi!\"";
    }

    @SuppressWarnings("deprecation")
    public ContactDTO getContactById(String id) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Contact.class);
        criteria.add(Restrictions.eq("id", Integer.parseInt(id)));
        Contact contact = (Contact) criteria.uniqueResult();
//        Contact contact = (Contact) session.createQuery("from Contact where id = " + id).uniqueResult();
        ContactDTO contactDTO = ContactDTO.ofDTO(contact);
        session.close();
        return contactDTO;
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public List<ContactDTO> getAllContacts() {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Contact.class);
        List<Contact> contactCriteria = criteria.list();
//        List<Contact> contacts = new ArrayList<Contact>(session.createQuery("from Contact").list());
        List<ContactDTO> contactDTOS = new ArrayList<>(contactCriteria != null ? contactCriteria.size() : 0);
        if (contactCriteria != null) {
            for (Contact phone : contactCriteria) {
                contactDTOS.add(ContactDTO.ofDTO(phone));
            }
        }
        session.close();
        return contactDTOS;
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public ContactDTO getContactWithPhotos(String id) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Contact.class);
        criteria.add(Restrictions.eq("id", Integer.parseInt(id)));
        Contact contact = (Contact) criteria.uniqueResult();
        Criteria criteria1 = session.createCriteria(Photo.class);
        criteria1.add(Restrictions.eq("contactId", contact.getId()));
        List<Photo> photos = criteria1.list();
        contact.setPhotosById(photos);
        ContactDTO contactDTO = null;
        if (contact != null) {
            contactDTO = ContactDTO.ofDTO(contact);
        }
        session.close();
        return contactDTO;
    }

    public ContactDTO savePhone(ContactDTO contactDTO) {
        Contact contact = Contact.of(contactDTO);
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(contact);
        session.getTransaction().commit();
        session.close();
        return null;
    }

    public ContactDTO deletePhone(ContactDTO contactDTO) {
        Contact contact = Contact.of(contactDTO);
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(contact);
        session.getTransaction().commit();
        session.close();
        return null;
    }

    public ContactDTO updatePhone(ContactDTO contactDTO) {
        Contact contact = Contact.of(contactDTO);
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(contact);
        session.getTransaction().commit();
        session.close();
        return null;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}