package svt.phoneBook.server;

import svt.phoneBook.client.dto.PhotoDTO;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

@MultipartConfig(location = "C:\\111")
public class PhotoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext servletContext = req.getServletContext();
        String name = req.getParameter("name");
        String fileName = "C:\\111\\";

        //адресс картинки в папке
        String mime = servletContext.getMimeType(fileName);
        resp.setContentType(mime);
        PhotoServiceImpl photoService = new PhotoServiceImpl();
        PhotoDTO photoDTO = photoService.getPhotoById(name);
            File file = new File(fileName.concat(photoDTO.getPhoto()));
            resp.setContentLength((int) file.length());
            FileInputStream in = new FileInputStream(file);
            OutputStream out = resp.getOutputStream();

            // копирует картинку в Outputstream
            byte[] buf = new byte[1024];
            int count;
            while ((count = in.read(buf)) >= 0) {
                out.write(buf, 0, count);
            }
            out.close();
            in.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int contactId = Integer.parseInt(req.getParameter("id"));
        Part filePart = req.getPart("photo");
        String extension = extractFileName(filePart);
        String fileName = String.valueOf(System.currentTimeMillis()).concat(extension);
//        Save photo on disk
        filePart.write(fileName);
//        Save photo to DB
        PhotoServiceImpl photoService = new PhotoServiceImpl();
        photoService.savePhoto(contactId, fileName);
    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("."), s.length()-1);
            }
        }
        return "";
    }
}
