package svt.phoneBook.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import svt.phoneBook.HibernateUtil;
import svt.phoneBook.client.LoginService;
import svt.phoneBook.shared.User;


public class LoginServiceImpl extends RemoteServiceServlet implements LoginService {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @SuppressWarnings("deprecation")
    @Override
    public String doLogin(String id, String password) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.eq("id", Integer.parseInt(id)));
        User user = (User) criteria.uniqueResult();
//        Contact contact = (Contact) session.createQuery("from Contact where id = " + id).uniqueResult();
        if(user != null && user.getPassword().equals(password)) {
            session.close();
            return "true:" + id;
        }
        session.close();
        return "false";
    }

    public User getUserById (String id) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM User where id = :id");
        query.setParameter("id", Integer.parseInt(id));
        return (User) query.uniqueResult();
    }
}
