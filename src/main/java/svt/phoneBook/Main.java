package svt.phoneBook;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import svt.phoneBook.client.dto.PhotoDTO;
import svt.phoneBook.server.PhotoServiceImpl;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();


        PhotoServiceImpl photoService = new PhotoServiceImpl();
        List<PhotoDTO> photoDTOS = photoService.getAllPhoto();
        for (PhotoDTO photoDTO : photoDTOS) {
            System.out.println(photoDTO);
        }

//        Contact contact = new Contact();
//        contact.setName("nam1e");
//        contact.setSurname("surname1");
//        contact.setPhoneNumber(3803000);
//        session.save(contact);

//        session.beginTransaction();
//        session.save(contact);
//        session.getTransaction().commit();
//        List<Contact> phones = new ArrayList<Contact>(session.createQuery("from Contact").list());
//        for (Contact phone : phones) {
//            System.out.println(phone);
//        }
        session.close();
        sessionFactory.close();
    }
}
