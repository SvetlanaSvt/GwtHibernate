package svt.phoneBook.shared;

import svt.phoneBook.client.dto.ContactDTO;
import svt.phoneBook.client.dto.PhotoDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "contact")
public class Contact implements Serializable{

    private static final long serialVersionUID = -5471293067070108157L;
    private int id;
    private String name;
    private String surname;
    private int phoneNumber;
    private List<Photo> photosById;

    public Contact() {
    }

    public static Contact of(ContactDTO contactDTO){
        Contact contact = new Contact();
        contact.setName(contactDTO.getName());
        contact.setSurname(contactDTO.getSurname());
        contact.setPhoneNumber(contactDTO.getPhoneNumber());
        List<PhotoDTO> photoDTOs = contactDTO.getPhotosById();
        if(photoDTOs != null) {
            List<Photo> photos = new ArrayList<>(photoDTOs.size());
            for (PhotoDTO photoDTO : photoDTOs) {
                photos.add(Photo.of(photoDTO));
            }
            contact.setPhotosById(photos);
        }

        return contact;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "phone_number")
    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @OneToMany(mappedBy = "contactByContactId", fetch = FetchType.LAZY)
    public List<Photo> getPhotosById() {
        return photosById;
    }

    public void setPhotosById(List<Photo> photosById) {
        this.photosById = photosById;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        if (id != contact.id) return false;
        if (phoneNumber != contact.phoneNumber) return false;
        if (name != null ? !name.equals(contact.name) : contact.name != null) return false;
        if (surname != null ? !surname.equals(contact.surname) : contact.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + phoneNumber;
        return result;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", photosById=" + photosById +
                '}';
    }
}
