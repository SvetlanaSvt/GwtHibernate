package svt.phoneBook.shared;

import svt.phoneBook.client.dto.PhotoDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "photo")
public class Photo implements Serializable{

    private static final long serialVersionUID = 6472128476671639319L;
    private int id;
    private String photo;
    private Integer contactId;
    private Contact contactByContactId;

    public Photo() {
    }

    public static Photo of(PhotoDTO photoDTO){
        Photo photo1 = new Photo();
        photo1.setId(photoDTO.getId());
        photo1.setContactId(photoDTO.getContactId());
        photo1.setPhoto(photoDTO.getPhoto());
        return photo1;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "photo")
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Basic
    @Column(name = "contact_id")
    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Photo photo1 = (Photo) o;

        if (id != photo1.id) return false;
        if (photo != null ? !photo.equals(photo1.photo) : photo1.photo != null) return false;
        if (contactId != null ? !contactId.equals(photo1.contactId) : photo1.contactId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (photo != null ? photo.hashCode() : 0);
        result = 31 * result + (contactId != null ? contactId.hashCode() : 0);
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public Contact getContactByContactId() {
        return contactByContactId;
    }

    public void setContactByContactId(Contact contactByContactId) {
        this.contactByContactId = contactByContactId;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "id=" + id +
                ", photo='" + photo + '\'' +
                ", contactId=" + contactId +
                '}';
    }
}
