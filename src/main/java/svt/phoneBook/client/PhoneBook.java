package svt.phoneBook.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import svt.phoneBook.client.dto.ContactDTO;
import svt.phoneBook.client.dto.PhotoDTO;
import svt.phoneBook.shared.User;

import java.util.List;

/**
 * Entry point classes define <code>onModuleLoad()</code>
 */
public class PhoneBook implements EntryPoint, ValueChangeHandler<String> {

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {

        RootPanel.get().add(new Login());
        History.addValueChangeHandler(this);
        History.fireCurrentHistoryState();

    }

    @Override
    public void onValueChange(ValueChangeEvent<String> event) {
        String historyToken = event.getValue();
        String[] tokens = historyToken.split(":");
        String history = tokens[0];
        switch (history){
            case "false":
                Window.alert("Incorrect user id or password!");
                RootPanel.get().clear();
                RootPanel.get("phoneBook").clear();
                getPhoneBook();
                break;
            case "showContactAndPhoto":
                RootPanel.get().clear();
                RootPanel.get("phoneBook").clear();
                showContactAndPhoto(tokens[1]);
                break;
            case "showAllPhoto":
                RootPanel.get().clear();
                RootPanel.get("phoneBook").clear();
                showAllPhotoPage();
                break;
            case "true":
                RootPanel.get().clear();
                RootPanel.get("phoneBook").clear();
                getPhoneBook();
                break;
            case "logOut":
                RootPanel.get().clear();
                RootPanel.get("phoneBook").clear();
                RootPanel.get().add(new Login());
                break;
        }
    }

    private void getPhoneBook() {
        final Label info = new Label("If you want to test admin enter id=1, password=1.");
        final Button logOut = new Button("LogOut");
        final Label firstNameLabel = new Label("Имя");
        final TextBox firstName = new TextBox();
        final Label lastNameLabel = new Label("Фамилия");
        final TextBox lastName = new TextBox();
        final Label phoneNumberLabel = new Label("Номер телефона");
        final TextBox phoneNumber = new TextBox();
        Button saveContact = new Button("Сохранить");
        final Button outputContact = new Button("Вывести список контактов");
        final FlexTable listOfContacts = new FlexTable();
        final FlexTable changeContactTable = new FlexTable();
        final FileUpload downloadPhoto = new FileUpload();
        final Button downloadBtn = new Button("Сохранить фото");
        FormPanel formPanelPhoto = new FormPanel();
        formPanelPhoto.setMethod(FormPanel.METHOD_POST);
        formPanelPhoto.setEncoding(FormPanel.ENCODING_MULTIPART);
        formPanelPhoto.setAction(GWT.getModuleBaseURL() + "PhotoServlet");
        downloadPhoto.setName("photo");
        Button showAllPhotoBtn = new Button("Показать все фото");

        showAllPhotoBtn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                String[] history = History.getToken().split(":");
                History.newItem("showAllPhoto:" + history[1]);
            }
        });

        logOut.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                String[] history = History.getToken().split(":");
                History.newItem("logOut");
            }
        });

//        set all Widget into one FlexTable
        final FlexTable saveContactTable = new FlexTable();
        saveContactTable.setWidget(0, 1, info);
        saveContactTable.setWidget(0, 2, logOut);
        saveContactTable.setWidget(1, 1, firstNameLabel);
        saveContactTable.setWidget(1, 2, firstName);
        saveContactTable.setWidget(2, 1, lastNameLabel);
        saveContactTable.setWidget(2, 2, lastName);
        saveContactTable.setWidget(3, 1, phoneNumberLabel);
        saveContactTable.setWidget(3, 2, phoneNumber);
        saveContactTable.setWidget(4, 1, saveContact);
        saveContactTable.setWidget(5, 1, outputContact);
        saveContactTable.setWidget(6, 1, showAllPhotoBtn);

        saveContact.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                final ContactDTO book = new ContactDTO();
                if(!phoneNumber.getValue().matches("\\d{7}")) {
                    Window.alert("Введите телефон в формате 7007070");
                }else {
                    book.setName(firstName.getValue());
                    book.setSurname(lastName.getValue());
                    book.setPhoneNumber(Integer.parseInt(phoneNumber.getValue()));
                    ContactService.App.getInstance().savePhone(book, new AsyncCallback<ContactDTO>() {
                        public void onFailure(Throwable caught) {
                            Window.alert("Новый контакт не был сохранен");
                        }

                        public void onSuccess(ContactDTO result) {
                            Window.alert("Контакт " + book.toString() + " был успешно сохранен");
                        }
                    });
                }
            }
        });

        outputContact.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                listOfContacts.clear();
                ContactService.App.getInstance().getAllContacts(new AsyncCallback<List<ContactDTO>>() {
                    public void onFailure(Throwable caught) {
                        Window.alert("Не удалось получить список контактов");
                    }

                    public void onSuccess(List<ContactDTO> result) {
                        listOfContacts.setText(0, 1, "Имя");
                        listOfContacts.setText(0, 2, "Фамилия");
                        listOfContacts.setText(0, 3, "Номер телефона");
                        listOfContacts.setText(0, 4, "Удалить контакт");
                        listOfContacts.setText(0, 5, "Изменить контакт");
                        listOfContacts.setText(0, 6, "Добавить фото");
                        listOfContacts.setText(0, 7, "Фото");
                        for (final ContactDTO contactDTO : result) {
                            listOfContacts.setText(contactDTO.getId(), 1, contactDTO.getName());
                            listOfContacts.setText(contactDTO.getId(), 2, contactDTO.getSurname());
                            listOfContacts.setText(contactDTO.getId(), 3, String.valueOf(contactDTO.getPhoneNumber()));
                            Button removeStockButton = new Button("Удалить");
                            removeStockButton.addClickHandler(new ClickHandler() {
                                public void onClick(ClickEvent event) {
                                    ContactService.App.getInstance().deletePhone(contactDTO, new AsyncCallback<ContactDTO>() {
                                        public void onFailure(Throwable caught) {
                                            Window.alert("Contact wasn't saved!");
                                        }
                                        public void onSuccess(ContactDTO result) {
                                            listOfContacts.removeAllRows();
                                            outputContact.click();
                                        }
                                    });
                                }
                            });
                            listOfContacts.setWidget(contactDTO.getId(), 4, removeStockButton);
                            Button updateContact = new Button("Изменить");
                            updateContact.addClickHandler(new ClickHandler() {
                                public void onClick(ClickEvent event) {
                                    changeContactTable.clear();
                                    final TextBox firstName = new TextBox();
                                    final TextBox lastName = new TextBox();
                                    final TextBox phoneNumber = new TextBox();
                                    firstName.setValue(contactDTO.getName());
                                    lastName.setValue(contactDTO.getSurname());
                                    phoneNumber.setValue(String.valueOf(contactDTO.getPhoneNumber()));
                                    Button saveChangesButton = new Button("Сохранить изменения");

                                    changeContactTable.setText(1, 1, "Имя");
                                    changeContactTable.setWidget(1, 2, firstName);
                                    changeContactTable.setText(2, 1, "Фамилия");
                                    changeContactTable.setWidget(2, 2, lastName);
                                    changeContactTable.setText(3, 1, "Номер телефона");
                                    changeContactTable.setWidget(3, 2, phoneNumber);
                                    changeContactTable.setWidget(4, 1, saveChangesButton);

                                    saveChangesButton.addClickHandler(new ClickHandler() {
                                        public void onClick(ClickEvent event) {
                                            final ContactDTO changingContact = new ContactDTO();
                                            changingContact.setId(contactDTO.getId());
                                            changingContact.setName(firstName.getValue());
                                            changingContact.setSurname(lastName.getValue());
                                            changingContact.setPhoneNumber(Integer.parseInt(phoneNumber.getValue()));

                                                ContactService.App.getInstance().updatePhone(changingContact, new AsyncCallback<ContactDTO>() {
                                                    public void onFailure(Throwable caught) {
                                                        Window.alert("Обновить контакт не удалось!");
                                                    }

                                                    public void onSuccess(ContactDTO result) {
                                                        outputContact.click();
                                                        changeContactTable.removeAllRows();
                                                    }
                                                });

                                        }
                                    });
                                }
                            });
                            listOfContacts.setWidget(contactDTO.getId(), 5, updateContact);
                            Button addPhoto = new Button("Добавить фото");
                            addPhoto.addClickHandler(new ClickHandler() {
                                public void onClick(ClickEvent event) {
                                    VerticalPanel panelPhoto = new VerticalPanel();
                                    Hidden idContactDto = new Hidden();
                                    idContactDto.setValue(String.valueOf(contactDTO.getId()));
                                    idContactDto.setName("id");
                                    formPanelPhoto.setWidget(panelPhoto);
                                    panelPhoto.add(idContactDto);
                                    panelPhoto.setSpacing(10);
                                    panelPhoto.add(downloadPhoto);
                                    panelPhoto.add(downloadBtn);
                                    downloadBtn.addClickHandler(new ClickHandler() {
                                        public void onClick(ClickEvent event) {
                                            if (downloadPhoto.getFilename().length() == 0) {
                                                Window.alert("Фото не выбрано!");
                                            } else {
                                                formPanelPhoto.submit();
                                                panelPhoto.clear();
                                            }
                                        }
                                    });

                                }
                            });
                            listOfContacts.setWidget(contactDTO.getId(), 6, addPhoto);
                            PhotoService.App.getInstance().getPhotoByContactId(contactDTO.getId(), new AsyncCallback<PhotoDTO>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    Window.alert("getPhotoByContactId " + caught.getMessage());
                                }

                                @Override
                                public void onSuccess(PhotoDTO photoDTO) {
                                    listOfContacts.setWidget(contactDTO.getId(), 7, getImage(photoDTO));
                                }
                            });
                            Button allPhotoOfContact = new Button("Показать все фото контакта");
                            allPhotoOfContact.addClickHandler(new ClickHandler() {
                                public void onClick(ClickEvent event) {
                                    History.newItem("showContactAndPhoto:" + contactDTO.getId());

                                }
                            });
                            listOfContacts.setWidget(contactDTO.getId(), 8, allPhotoOfContact);
                        }
                    }
                });
            }
        });

        saveContactTable.setWidget(7, 2, formPanelPhoto);
        saveContactTable.setWidget(8, 2, changeContactTable);
        saveContactTable.setWidget(9, 2, listOfContacts);
        RootPanel.get("phoneBook").add(saveContactTable);
    }

    private void showContactAndPhoto (String id) {
        RootPanel.get().clear();
        RootPanel.get("phoneBook").clear();
        ContactService.App.getInstance().getContactWithPhotos(id, new AsyncCallback<ContactDTO>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }

            @Override
            public void onSuccess(ContactDTO contactDTO) {
                FlexTable contactWithPhoto = new FlexTable();
                contactWithPhoto.setText(contactDTO.getId(), 1, String.valueOf(contactDTO.getId()));
                contactWithPhoto.setText(contactDTO.getId(), 2, contactDTO.getName());
                contactWithPhoto.setText(contactDTO.getId(), 3, contactDTO.getSurname());
                contactWithPhoto.setText(contactDTO.getId(), 4, String.valueOf(contactDTO.getPhoneNumber()));

                List<PhotoDTO> photoDTOS = contactDTO.getPhotosById();
                HorizontalPanel panelOfPhotos = new HorizontalPanel();
                panelOfPhotos.setSpacing(5);
                if (photoDTOS != null && !photoDTOS.isEmpty()) {
                    for (PhotoDTO photoDTO : photoDTOS) {
                        Image image = new Image();
                        image.setUrl(GWT.getModuleBaseURL() + "PhotoServlet" + "?name=" + photoDTO.getId());
                        image.setSize("100", "100");
                        panelOfPhotos.add(image);
                    }
                } else {
                    Image image = new Image();
                    image.setUrl("../image/no-photo.jpg");
                    panelOfPhotos.add(image);
                }
                contactWithPhoto.setWidget(contactDTO.getId(), 5, panelOfPhotos);
                RootPanel.get("phoneBook").add(contactWithPhoto);
            }
        });
    }

    private void showAllPhotoPage() {
        String[] history = History.getToken().split(":");
        LoginService.App.getInstance().getUserById(history[1], new AsyncCallback<User>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }

            @Override
            public void onSuccess(User result) {
                if(result.getRole().equals("Admin")) {
                    PhotoService.App.getInstance().getAllPhoto(new AsyncCallback<List<PhotoDTO>>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            Window.alert(caught.getMessage());
                        }

                        @Override
                        public void onSuccess(List<PhotoDTO> photoDTOs) {
                            HorizontalPanel horizontalPanel = new HorizontalPanel();
                            if (photoDTOs != null && !photoDTOs.isEmpty()) {
                                for (PhotoDTO photoDTO : photoDTOs) {
                                    horizontalPanel.add(getImage(photoDTO));
                                }
                            } else {
                                horizontalPanel.add(new Label("There are no photo yet."));
                            }
                            RootPanel.get("phoneBook").add(horizontalPanel);
                        }
                    });
                } else {
                    Window.alert("У Вас нет прав доступа для просмотра всех фото");
                    History.back();
                }
            }
        });


    }

    private Image getImage(PhotoDTO photoDTO) {
        Image image = new Image();
        if (photoDTO.getPhoto() != null) {
            image.setUrl(GWT.getModuleBaseURL() + "PhotoServlet" + "?name=" + photoDTO.getId());
            image.setSize("100", "100");
        } else {
            image.setUrl("../image/no-photo.jpg");
            image.setSize("100", "100");
        }
        return image;
    }

}
