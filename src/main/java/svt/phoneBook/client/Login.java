package svt.phoneBook.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import svt.phoneBook.client.registration.Registration;

public class Login extends Composite {
    private static LoginUiBinder uiBinder = GWT.create(LoginUiBinder.class);

    @UiTemplate("Login.ui.xml")
    interface LoginUiBinder extends UiBinder<Widget, Login> {
    }

    public Login() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @UiField
    TextBox loginBox;

    @UiField
    TextBox passwordBox;

    @UiHandler("buttonRegister")
    void onRegisterClick (ClickEvent event) {
        RootPanel.get().clear();
        RootPanel.get().add(new Registration());
    }

    @UiHandler("buttonSubmit")
    void doClickSubmit(ClickEvent event) {

        LoginService.App.getInstance().doLogin(loginBox.getValue(), passwordBox.getValue(), new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {
                Window.alert("Login doesn't work");
            }

            public void onSuccess(String result) {
                History.newItem(result);
            }
        });
    }

    @UiHandler("loginBox")
    void handleLoginChange(ValueChangeEvent<String> event) {
        loginBox.getValue();
    }

    @UiHandler("passwordBox")
    void handlePasswordChange(ValueChangeEvent<String> event) {
        passwordBox.getValue();
    }
}
