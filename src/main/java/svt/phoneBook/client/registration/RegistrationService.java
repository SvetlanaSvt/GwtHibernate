package svt.phoneBook.client.registration;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("RegistrationService")
public interface RegistrationService extends RemoteService {
    class App {
        private static RegistrationServiceAsync ourInstance = GWT.create(RegistrationService.class);

        public static synchronized RegistrationServiceAsync getInstance() {
            return ourInstance;
        }
    }

    String saveNewUser(String name, String password);
}
