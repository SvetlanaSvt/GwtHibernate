package svt.phoneBook.client.registration;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class Registration extends Composite {
    private static RegistrationUiBinder uiBinder = GWT.create(RegistrationUiBinder.class);

    @UiTemplate("Registration.ui.xml")
    interface RegistrationUiBinder extends UiBinder<Widget, Registration> {
    }

    @UiField
    TextBox nameBox;

    @UiField
    TextBox passwordBox;

    @UiField
    TextBox confirmPasswordBox;

    @UiField
    Label error;

    @UiHandler("buttonSubmit")
    void onRegisterClick (ClickEvent event) {
        if (!passwordBox.getText().equals(confirmPasswordBox.getText())) {
            error.setText("The password and confirm password is doesn't match");
            error.setVisible(true);
            return;
        }
        RegistrationService.App.getInstance().saveNewUser(nameBox.getText(), passwordBox.getText(), new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Can't save user");
            }

            @Override
            public void onSuccess(String result) {
                Window.alert("Please, remember your login id = " + result + ".");
            }
        });
    }

    public Registration() {
        initWidget(uiBinder.createAndBindUi(this));
    }
}
