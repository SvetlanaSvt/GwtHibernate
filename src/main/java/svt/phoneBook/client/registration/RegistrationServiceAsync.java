package svt.phoneBook.client.registration;


import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RegistrationServiceAsync {
    void saveNewUser(String name, String password, AsyncCallback<String> async);
}
