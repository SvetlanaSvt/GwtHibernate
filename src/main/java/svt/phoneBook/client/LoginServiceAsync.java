package svt.phoneBook.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import svt.phoneBook.shared.User;

public interface LoginServiceAsync {
    void doLogin(String id, String password, AsyncCallback<String> async);

    void getUserById(String id, AsyncCallback<User> async);
}
