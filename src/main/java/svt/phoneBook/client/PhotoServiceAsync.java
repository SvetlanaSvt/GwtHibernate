package svt.phoneBook.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import svt.phoneBook.client.dto.PhotoDTO;

import java.util.List;

public interface PhotoServiceAsync {
    void getAllPhoto(AsyncCallback<List<PhotoDTO>> async);

    void getPhotoById(String id, AsyncCallback<PhotoDTO> async);

    void getMessage(String msg, AsyncCallback<String> async);

    void getPhotoByContactId(int id, AsyncCallback<PhotoDTO> async);
}
