package svt.phoneBook.client.dto;

import svt.phoneBook.shared.Contact;
import svt.phoneBook.shared.Photo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ContactDTO implements Serializable {

    private static final long serialVersionUID = 3725911978044256029L;
    private int id;
    private String name;
    private String surname;
    private int phoneNumber;
    private List<PhotoDTO> photosById;

    public ContactDTO() {
    }

    public static ContactDTO ofDTO(Contact contact) {
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setId(contact.getId());
        contactDTO.setName(contact.getName());
        contactDTO.setSurname(contact.getSurname());
        contactDTO.setPhoneNumber(contact.getPhoneNumber());
        List<Photo> photos = contact.getPhotosById();
        if(photos != null) {
            List<PhotoDTO> photoDTOs = new ArrayList<PhotoDTO>(photos.size());
            for (Photo photo : photos) {
                photoDTOs.add(PhotoDTO.ofDTO(photo));
            }
            contactDTO.setPhotosById(photoDTOs);
        }
        return contactDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<PhotoDTO> getPhotosById() {
        return photosById;
    }

    public void setPhotosById(List<PhotoDTO> photosById) {
        this.photosById = photosById;
    }

    @Override
    public String toString() {
        return "Имя: " + name +
                ", фамилия: " + surname +
                ", номер телефона: " + phoneNumber;
    }
}
