package svt.phoneBook.client.dto;

import svt.phoneBook.shared.Photo;

import java.io.Serializable;

public class PhotoDTO implements Serializable {

    private static final long serialVersionUID = 2328263679964410120L;
    private int id;
    private String photo;
    private Integer contactId;
    private ContactDTO contactDTO;

    public PhotoDTO() {
    }

    public static PhotoDTO ofDTO(Photo photo) {
        PhotoDTO photoDTO = new PhotoDTO();
        photoDTO.setId(photo.getId());
        photoDTO.setPhoto(photo.getPhoto());
        photoDTO.setContactId(photo.getContactId());
        System.out.println();
        return photoDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public ContactDTO getContactDTO() {
        return contactDTO;
    }

    public void setContactDTO(ContactDTO contactDTO) {
        this.contactDTO = contactDTO;
    }

    @Override
    public String toString() {
        return "PhotoDTO{" +
                "id=" + id +
                ", photo='" + photo + '\'' +
                ", contactId=" + contactId +
                '}';
    }
}
