package svt.phoneBook.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import svt.phoneBook.shared.User;

@RemoteServiceRelativePath("LoginService")
public interface LoginService extends RemoteService {

    class App {
        private static LoginServiceAsync ourInstance = GWT.create(LoginService.class);

        public static synchronized LoginServiceAsync getInstance() {
            return ourInstance;
        }
    }

    String doLogin(String id, String password);
    User getUserById(String id);
}
