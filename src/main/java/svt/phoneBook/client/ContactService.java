package svt.phoneBook.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import svt.phoneBook.client.dto.ContactDTO;

import java.util.List;

@RemoteServiceRelativePath("ContactService")
public interface ContactService extends RemoteService {

    String getMessage(String msg);

    class App {
        private static ContactServiceAsync ourInstance = GWT.create(ContactService.class);

        public static synchronized ContactServiceAsync getInstance() {
            return ourInstance;
        }
    }
    ContactDTO getContactById(String id);
    List<ContactDTO> getAllContacts();
    ContactDTO savePhone(ContactDTO phoneBook);
    ContactDTO deletePhone(ContactDTO phoneBook);
    ContactDTO updatePhone(ContactDTO phoneBook);
    ContactDTO getContactWithPhotos(String id);
}
