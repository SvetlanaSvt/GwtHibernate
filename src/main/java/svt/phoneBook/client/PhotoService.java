package svt.phoneBook.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import svt.phoneBook.client.dto.PhotoDTO;

import java.util.List;

@RemoteServiceRelativePath("PhotoService")
public interface PhotoService extends RemoteService {

    String getMessage(String msg);
    class App {
        private static PhotoServiceAsync ourInstance = GWT.create(PhotoService.class);

        public static synchronized PhotoServiceAsync getInstance() {
            return ourInstance;
        }
    }
    PhotoDTO getPhotoById(String id);
    List<PhotoDTO> getAllPhoto();
    PhotoDTO getPhotoByContactId(int id);
}
