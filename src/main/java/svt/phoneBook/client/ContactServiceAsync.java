package svt.phoneBook.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import svt.phoneBook.client.dto.ContactDTO;

import java.util.List;

public interface ContactServiceAsync {

    void getMessage(String msg, AsyncCallback<String> async);

    void getAllContacts(AsyncCallback<List<ContactDTO>> async);

    void savePhone(ContactDTO phoneBook, AsyncCallback<ContactDTO> async);

    void deletePhone(ContactDTO phoneBook, AsyncCallback<ContactDTO> async);

    void updatePhone(ContactDTO phoneBook, AsyncCallback<ContactDTO> async);

    void getContactById(String id, AsyncCallback<ContactDTO> async);

    void getContactWithPhotos(String id, AsyncCallback<ContactDTO> async);
}
